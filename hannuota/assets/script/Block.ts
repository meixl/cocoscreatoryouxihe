import { Constant } from "./game/Constant";
import CustomerEventListener from "./game/CustomerEventListener";
import RunTimeData from "./game/RunTimeData";
import Game from "./game";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Block extends cc.Component {

    private currBlockIndex: string = '';

    // 当前方块汉诺塔杠数
    private currTowerIndex: number = 0;

    onLoad(): void {
        this.node.on(cc.Node.EventType.TOUCH_START, this.touchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.touchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.touchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.touchEnd, this);
    }

    start(): void {

    }

    touchStart(): void {
        console.log('touchStart')
        // 每次触摸, 先校验是否可移动
        this.currBlockIndex = this.node.getComponent(cc.Sprite).spriteFrame.name;
        const blockPosition = RunTimeData.getInstance().blockPosition;

        let canMove = false;
        for (let i = 0; i < blockPosition.length; i++) {
            if (blockPosition[i][0] == this.currBlockIndex) {
                canMove = true;
                break;
            }
        }

        if (canMove) {
            console.log('最上面的, 可移动');
            RunTimeData.getInstance().orignPos = this.node.position;
            this.node.on(cc.Node.EventType.TOUCH_MOVE, this.touchMove, this);
            this.node.on(cc.Node.EventType.TOUCH_END, this.touchEnd, this);
            this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.touchEnd, this);

            CustomerEventListener.on(Constant.EventName.BLOCK_MOVE_FIX, this.moveFix, this);
        } else {
            console.log('非最上面的, 不可移动');
            this.node.off(cc.Node.EventType.TOUCH_MOVE, this.touchMove, this);
            this.node.off(cc.Node.EventType.TOUCH_END, this.touchEnd, this);
            this.node.off(cc.Node.EventType.TOUCH_CANCEL, this.touchEnd, this);

            CustomerEventListener.off(Constant.EventName.BLOCK_MOVE_FIX, this.moveFix, this);
        }
    }

    touchMove(e: cc.Touch): void {
        // 移动方块
        let delta = e.getDelta();
        this.node.x += delta.x;
        this.node.y += delta.y;
    }

    /**
     * 方块移动后, 结束触屏
     */
    touchEnd(): void {
        let pos = this.node.position;
        this.currBlockIndex = this.node.getComponent(cc.Sprite).spriteFrame.name;
        CustomerEventListener.dispatchEvent(Constant.EventName.BLOCK_MOVE_END, this.currBlockIndex, pos, this.currTowerIndex);
    }

    /**
     * 方块可以移动后, 方块位置
     * 
     * @param args 修正后方块位置
     */
    moveFix(...args: any[]): void {

        // 获取 index, 若当前 block 就是被选择的, 进行操作
        let blockIndex = args[0];
        const blockPos = args[1];

        if (blockIndex !== this.currBlockIndex) {
            return;
        }

        this.node.x = blockPos.x;
        this.node.y = blockPos.y;

        if (args[2]) {
            const towerIndex = args[2];
            this.currTowerIndex = towerIndex;
        }
    }

}