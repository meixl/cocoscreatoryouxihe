import { Constant } from "./game/Constant";
import CustomerEventListener from "./game/CustomerEventListener";
import RunTimeData from "./game/RunTimeData";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Game extends cc.Component {

    @property({ type: cc.Node, tooltip: '汉诺塔-摆放层' })
    towerLayer: cc.Node = null;

    @property({ type: cc.Node, tooltip: '汉诺塔-模块摆放层' })
    blockLayer: cc.Node = null;

    @property({ type: cc.Integer, tooltip: '汉诺塔-吸附范围' })
    towerContainPos: number = 100;

    @property({ type: cc.Prefab, tooltip: '汉诺塔模块-预制体' })
    blockPrefab: cc.Prefab = null;

    @property({ type: cc.Integer, tooltip: '汉诺塔模块-最低点' })
    blockYPos: number = 0;

    @property({ type: cc.Integer, tooltip: '汉诺塔模块-宽度' })
    blockMinWidth: number = 0;

    @property({ type: cc.Integer, tooltip: '汉诺塔模块-递增宽度' })
    blockIncrWidth: number = 0;

    @property({ type: cc.Integer, tooltip: '汉诺塔模块-高度' })
    blockHeight: number = 0;

    @property({ type: [cc.SpriteFrame], tooltip: '汉诺塔-图片集' })
    blockSpriteFrameList: cc.SpriteFrame[] = [];

    // 汉诺塔模块-递增高度
    blockIncrYPos: number = 0;

    // 3 个汉诺塔 x 坐标
    bolckXPos: number[] = [];

    private static instance: Game = null;

    onLoad(): void {

        if (Game.instance == null) {
            Game.instance = this;
        } else {
            this.destroy();
            return;
        }

        this.blockIncrYPos = this.blockHeight - 2;
    }

    start(): void {

        let towerList = this.towerLayer.children;

        for (let i = 0; i < towerList.length; i++) {
            this.bolckXPos[i] = towerList[i].x;
        }

        // 初始化模块
        let currentLevel = RunTimeData.getInstance().currentLevel;

        let arr = new Array();

        // 汉诺塔最少 3 个
        for (let i = 0; i < currentLevel + 2; i++) {
            console.log('init, i: ' + i)
            let block = cc.instantiate(this.blockPrefab);
            block.x = this.bolckXPos[0];
            block.y = this.blockYPos + this.blockIncrYPos * (currentLevel + 1 - i);
            block.width = this.blockMinWidth + this.blockIncrWidth * 2 * i;
            block.height = this.blockHeight;
            block.getComponent(cc.Sprite).spriteFrame = this.blockSpriteFrameList[i];
            this.blockLayer.addChild(block);

            // 位置记录
            RunTimeData.getInstance().blockPosition[0].push(i + '');

        }

        // console.log('ddddddd ' + RunTimeData.getInstance().blockPosition[0]);
        // console.log('ddddddd ' + RunTimeData.getInstance().blockPosition[1]);
        // RunTimeData.getInstance().blockPosition[0].shift();
        // RunTimeData.getInstance().blockPosition[1].unshift('3');
        // RunTimeData.getInstance().blockPosition[1].unshift('4');
        console.log('eeeeeee: ' + RunTimeData.getInstance().blockPosition[0]);
        console.log('eeeeeee: ' + RunTimeData.getInstance().blockPosition[1]);

        CustomerEventListener.on(Constant.EventName.BLOCK_MOVE_END, this.checkBlock, this);

    }

    public static log() {

    }

    /**
     * 触屏结束, 移动方块
     * 
     * @param args 方块位置, 当前移动的方块
     */
    private checkBlock(...args: any[]) {

        let blockIndex: string = args[0];
        let pos: cc.Vec3 = args[1];
        let towerIndex: number = args[2];
        let xPos = pos.x;

        let towerList = this.towerLayer.children;
        let movePermiss: boolean = false;
        // 判断是否在塔吸附范围内, 在就 x 轴吸附
        for (let i = 0; i < towerList.length; i++) {
            if (this.towerContainPos >= Math.abs(towerList[i].x - xPos)) {
                movePermiss = true;
                console.log('准许摆放: ' + i);
                pos.x = towerList[i].x;

                // 修改汉诺塔位置数据
                console.log('unshift(+): i=' + i + ", blockIndex=" + blockIndex);
                RunTimeData.getInstance().blockPosition[i].unshift(blockIndex);
                console.log('shift(-): towerIndex=' + towerIndex);
                RunTimeData.getInstance().blockPosition[towerIndex].shift();

                this.moveBlock(blockIndex, pos, i);

                break;
            }
        }

        // 不准许摆放, 移动回原来位置
        if (!movePermiss) {
            this.moveBlock(blockIndex, RunTimeData.getInstance().orignPos);
        } else {
            console.log('after move, xx: ' + RunTimeData.getInstance().blockPosition[0])
            console.log('after move, xx: ' + RunTimeData.getInstance().blockPosition[1])
            console.log('after move, xx: ' + RunTimeData.getInstance().blockPosition[2])
        }

    }

    private checkBlockYPos() {

    }

    private moveBlock(receivedBlockIndex: string, blockPos: cc.Vec3, newTowerIndex?: number, cb?: Function) {
        CustomerEventListener.dispatchEvent(Constant.EventName.BLOCK_MOVE_FIX, receivedBlockIndex, blockPos, newTowerIndex);
        if (cb) {
            cb();
        }
    }

    private countPos(): cc.Vec3 {
        return new cc.Vec3();
    }

    private updateBlockData() {

    }

}
