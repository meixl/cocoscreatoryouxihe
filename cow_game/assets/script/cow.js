var cow_skin = cc.Class({
    name: "cow_skin",
    properties: {
        cow_anim: {
            type: cc.SpriteFrame,
            default: []
        }
    }
});

cc.Class({
    extends: cc.Component,

    properties: {
        cow_skin_set: {
            type: cow_skin,
            default: []
        }
    },

    // onLoad () {},

    start() {
        // 牛的类型
        this.cow_type = Math.random() * 3;
        this.cow_type = Math.ceil(this.cow_type);
        if (this.cow_type === 3) {
            this.cow_type = 2;
        }
        // 帧动画
        this.anim_com = this.node.addComponent("frame_anim");
        console.log('anim_com: ' + anim_com);
        // 播放动画
        this.play_cow_walk();
    },

    play_cow_walk() {
        this.anim_com.sprite_frames = this.cow_skin_set[this.cow_type].cow_anim;
        this.anim_com.duration = 0.2;
        this.anim_com.play_loop();
    }

    // update (dt) {},
});
