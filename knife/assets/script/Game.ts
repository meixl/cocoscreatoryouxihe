// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    targetNode: cc.Node = null;

    @property(cc.Node)
    knifeNode: cc.Node = null;

    @property(cc.Prefab)
    knifePrefab: cc.Prefab = null;

    private targetRotation: number = 90;

    private canThrow: boolean = true;

    private knifeNodeArray: Array<cc.Node> = [];

    private gap: number = 15;

    start(): void {

        this.targetNode.zIndex=100;

        this.node.on(cc.Node.EventType.TOUCH_START, this.touchStart, this);
    }

    touchStart(): void {

        let r = this.targetNode.width / 2;
        console.log('圆盘半径: ' + r)

        // 当飞刀扔出时, 不可再扔
        if (this.canThrow) {
            this.canThrow = false;
            this.knifeNode.runAction(
                cc.sequence(
                    // 飞刀扔出
                    cc.moveTo(0.16, cc.v2(this.knifeNode.x, this.targetNode.y - this.targetNode.height / 2)),
                    // 底部位置重新生成新的飞刀
                    cc.callFunc(() => {

                        let isHit =false;
                        // 遍历刀子, 正负夹角即碰撞
                        for (let knifeNode of this.knifeNodeArray) {
                            console.log('angle: '+ knifeNode.angle)
                            if  (knifeNode.angle < this.gap || (360 - knifeNode.angle) < this.gap)  {
                                isHit =true;
                                break
                            }
                        }

                        if (isHit) {
                           // TODO 飞刀弹出动画
                           cc.director.loadScene('game')
                        }else {
                            let knifeNode = cc.instantiate(this.knifePrefab);
                            knifeNode.setPosition(this.knifeNode.position);
                            this.node.addChild(knifeNode);
                            this.knifeNode.setPosition(cc.v2(0, -300));
    
                            this.knifeNodeArray.push(knifeNode);
    
                            this.canThrow = true;
                        }

                       
                    })
                )
            );
        }
    }

    update(dt): void {
        this.targetNode.angle += this.targetRotation * dt;

        let r = this.targetNode.width / 2;

        for (let knifeNode of this.knifeNodeArray) {
            knifeNode.angle += this.targetRotation * dt;

            let rad = Math.PI * (knifeNode.angle - 90) / 180;

            knifeNode.x = this.targetNode.x + r * Math.cos(rad);
            knifeNode.y = this.targetNode.y + r * Math.sin(rad);
            // console.log("x: " + knifeNode.x + ", y: " + knifeNode.y);
        }
    }

}
