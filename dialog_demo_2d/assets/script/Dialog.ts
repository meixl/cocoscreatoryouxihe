const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property({ type: cc.Node })
    dialog: cc.Node = null;

    @property({ type: cc.Node })
    dialogContent: cc.Node = null;

    @property({ type: cc.Node })
    dialogMask: cc.Node = null;

    @property({ type: cc.Label })
    title: cc.Label = null;

    @property({ type: cc.Label })
    content: cc.Label = null;

    start() {
        this.title.getComponent(cc.Label).string = '222222'
        this.content.getComponent(cc.Label).string = '333333333333333333'
    }

    showDialog(): void {

        this.dialog.active = true;

        // 对话框遮罩层
        this.dialogMask.opacity = 0;
        let action1 = cc.fadeTo(0.2, 200);
        this.dialogMask.runAction(action1);

        // 对话框主题, 有个缩放的动画
        this.dialogContent.setScale(0, 0);
        let action2 = cc.scaleTo(0.2, 1).easing(cc.easeBackOut());
        this.dialogContent.runAction(action2);

    }

    closeDialog(): void {

        // 对话框遮罩层
        let action1 = cc.fadeTo(0.2, 0);
        this.dialogMask.runAction(action1);

        // 对话框主题, 有个缩放的动画
        let action2 = cc.scaleTo(0.2, 0).easing(cc.easeBackIn());
        let endF = cc.callFunc(() => {
            this.dialog.active = false;
        });

        let seq = cc.sequence([action2, endF]);

        this.dialogContent.runAction(seq);

    }


}
