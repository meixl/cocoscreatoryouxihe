
cc.Class({
    extends: cc.Component,

    properties: {
        gen_sound: {
            type: cc.AudioClip,
            default: null
        },
        boom_sound: {
            type: cc.AudioClip,
            default: null
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {

        // 等级, 已运行事件, 气球是否向上运动
        this.level = 1;
        this.past_time = 0;

        //
        this.balloon_animation = this.node.getChildByName("balloon_animation");
        this.blingbling_animation = this.node.getChildByName("blingbling_animation");
        // this.gen_balloon(3, 200);

        // 气球触碰
        this.node.on(cc.Node.EventType.TOUCH_START, function (e) {
            // 阻止事件冒泡
            e.stopPropagationImmediate();
            // 气球爆炸
            // if (!this.is_moving) {
            //     return;
            // }
            this.boom_balloon();
        }.bind(this), this.node)
    },

    start() {

    },

    gen_balloon(type, speed) {

        if (type < 1 || type > 7) {
            return;
        }

        // 移动：否
        this.is_moving = false;
        this.speed = speed;
        this.balloon_type = type;

        // 气球生成动画
        var ske_com = this.balloon_animation.getComponent(sp.Skeleton);
        ske_com.setAnimation(0, "animation_" + type + "_stop", true);

        // 气球生成闪烁动画
        ske_com = this.blingbling_animation.getComponent(sp.Skeleton);
        ske_com.setAnimation(0, "animation", false);

        // 动画配置及运行
        this.node.scale = 0;
        var scale = cc.scaleTo(0.5, Math.random() * 0.4 + 0.5).easing(cc.easeBackOut());
        var move_func = cc.callFunc(function () {
            this.is_moving = true;
        }.bind(this), this.node);

        var seq = cc.sequence([scale, move_func]);
        this.node.runAction(seq);

        // 气球生成音效
        cc.audioEngine.playMusic(this.gen_sound, false)

    },

    // 气球爆炸
    boom_balloon() {

        // 气球停止向上移动
        this.is_moving = false;
        let stopY = this.node.y;
        this.node.y = stopY;

        // 爆炸音效
        var ske_com = this.balloon_animation.getComponent(sp.Skeleton);
        ske_com.setAnimation(0, "animation_" + this.balloon_type + "_click", false);

        // 爆炸音效
        cc.audioEngine.playMusic(this.boom_sound, false)

        // 当爆炸动画结束后，我们应该删除这个节点
        ske_com.setCompleteListener(function () {
            this.node.removeFromParent();
        }.bind(this));
    },

    update(dt) {
        // 气球动起来
        if (this.is_moving) {
            var sy = this.speed * dt;
            this.node.y += sy
        } else {
            let stopY = this.node.y;
            this.node.y = stopY
            console.log('stop moving')
        }
        // 太远及时消除
        if (this.node.y > 1200) {
            this.node.removeFromParent();
        }
    },

});
