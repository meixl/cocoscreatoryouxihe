const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property({ type: cc.Node, tooltip: "被控制节点" })
    player: cc.Node = null;

    onLoad() {
        // cc.systemEvent.on(cc.Node.EventType.TOUCH_START, this.touchStart, this);
        // cc.systemEvent.on(cc.Node.EventType.TOUCH_MOVE, this.touchMove, this);

        console.log('player: ' + this.player);
        // this.node.on(cc.Node.EventType.TOUCH_START, this.touchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.touchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.touchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.touchEnd, this);
    }

    start() {

    }

    onDestroy() {
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this.touchMove, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this.touchEnd, this);
        this.node.off(cc.Node.EventType.TOUCH_CANCEL, this.touchEnd, this);
    }

    touchStart(e: cc.Touch) {
        let worldPos: cc.Vec2 = e.getLocation();
        let localPos: cc.Vec2 = this.node.convertToNodeSpaceAR(worldPos);
        this.player.setPosition(localPos);
    }

    touchMove(e: cc.Touch) {
        this.player.setPosition(this.player.getPosition().add(e.getDelta()));
    }

    touchEnd(e: cc.Touch) {
        console.log('移开');
    }

}
