
const { ccclass, property } = cc._decorator;

@ccclass
export default class EnablePhy extends cc.Component {

    @property({ tooltip: "是否开启物理引擎" })
    enablePhy: boolean = false;

    @property({ tooltip: "是否调试模式" })
    isDebug: boolean = false;

    @property({ tooltip: "重力加速度" })
    gravity: cc.Vec2 = cc.v2(0, -320);

    private phyManager: cc.PhysicsManager;

    onLoad() {
        if (this.enablePhy) {
            this.phyManager = cc.director.getPhysicsManager();
            this.phyManager.enabled = this.enablePhy;
            if (this.isDebug) {
                this.phyManager.debugDrawFlags = cc.PhysicsManager.DrawBits.e_shapeBit || cc.PhysicsManager.DrawBits.e_jointBit;
            } else {
                this.phyManager.debugDrawFlags = 0;
            }
            //console.log("系统默认重力加速度：",this.phyManager.gravity);
            this.phyManager.gravity = this.gravity;
        } else {
            this.phyManager.enabled = false;
        }
    }
}
