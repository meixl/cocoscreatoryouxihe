import GameCtrl from "./GameCtrl";

const { ccclass, property } = cc._decorator;

@ccclass
export default class BulletCtrl extends cc.Component {

    private speed: number = 1500;

    private killValue: number = 300;

    private isActive = true;

    initBulletConfig() {
        // console.log('initBulletConfig')
        // this.speed = speed;
        // this.killValue = killValue;
        this.isActive = true;
    }

    update(dt: number): void {
        if (!this.isActive) {
            return;
        }
        this.node.y += this.speed * dt;
        // 回收子弹
        if (this.node.y >= cc.winSize.height / 2 + this.node.height / 2) {
            this.isActive = false;
            GameCtrl.instance.freeBullet(this.node);
        }
    }

}